package learn.springboot.basics.application.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = { "learn-springboot-basics" })
@RestController
@RequestMapping(value = { "/api/private/learn/springboot/basics" })
public class PrivateLearnSpringBootBasicsController {

    @ApiOperation(value = "getHardCodedHelloString", notes = "Gets Hard Coded Hello String.")
    @GetMapping(path = "/getHardCodedHelloString")
    public ResponseEntity<String> getHardCodedHelloString() {
        return ResponseEntity.ok("Hello");
    }
}
