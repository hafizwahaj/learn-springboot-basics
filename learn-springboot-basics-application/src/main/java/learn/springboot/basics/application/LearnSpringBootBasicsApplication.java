package learn.springboot.basics.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnSpringBootBasicsApplication {

    public static void main(String[] args){
        SpringApplication.run(LearnSpringBootBasicsApplication.class, args);
    }
}
